# Массив
set myVariable {red green blue}
puts [lindex $myVariable 2]
set myVariable "red green blue"
puts [lindex $myVariable 2]
# Ассоциатив массив
puts "--------------"
set  marks(english) 80
puts $marks(english)
set  marks(mathematics) 90,100
puts $marks(mathematics)
# О эти странные переменные
puts "--------------"
set variableA 10
set {variable B} test
puts $variableA
puts ${variable B}
# Математика и динамические типы
puts "--------------"
set variableA "10"
puts $variableA
set sum [expr $variableA +20];
puts $sum
# Унарные операторы
puts "--------------"
set a 10;
set b [expr $a == 1 ? 20: 30]
puts "Value of b is $b\n"
set b [expr $a == 10 ? 20: 30]
puts "Value of b is $b\n"
#Массивы подробнее
puts "--------------"
set languages(0) Tcl
set languages(1) "C Language"
puts $languages(0)
puts $languages(1)
# Итерации массива
puts "--------------"
set languages(0) Tcl
set languages(1) "C Language"
for { set index 0 }  { $index < [array size languages] }  { incr index } {
   puts "languages($index) : $languages($index)"
}
# Строки Стринг
puts "--------------"
set s1 "Hello"
set s2 "World"
set s3 "World"
puts [string compare s1 s2]
if {[string compare s2 s3] == 0} {
   puts "String \'s1\' and \'s2\' are same.";
}

if {[string compare s1 s2] == -1} {
   puts "String \'s1\' comes before \'s2\'.";
}

if {[string compare s2 s1] == 1} {
   puts "String \'s2\' comes before \'s1\'.";
}
# Append
set s1 "Hello"
append s1 " World"
puts $s1
# Форматирование
puts [format "%f" 43.5]
puts [format "%e" 43.5]
puts [format "%d %s" 4 tuts]
puts [format "%s" "Tcl Language"]
puts [format "%x" 40]
# Списки List
puts "-------Списки List-------"
set colorList1 {red green blue}
set colorList2 [list red green blue]
set colorList3 [split "red_green_blue" _]
puts $colorList1
puts $colorList2
puts $colorList3
# Магия
puts "------Магия--------"
set var orange
append var " " "blue"
lappend var "red"
lappend var "green"
puts $var
# Размер списка
puts "--------------"
set var {orange blue red green}
puts [llength $var]
# Замена элементов в списке
puts "------Замена элементов в списке--------"
set var {orange blue red green}
set var [lreplace $var 2 3 black white]
puts $var
# иная вариация замены
puts "----- иная вариация замены---------"
set var {orange blue red green}
lset var 0 black
puts $var
# Сортировка
puts "----- Сортировка---------"
set var {orange blue red green}
set var [lsort $var]
puts $var
