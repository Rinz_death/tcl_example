package require Tk
set host localhost
set port 7654

 # Make the GUI
 labelframe .top -text "Output"
 labelframe .bottom -text "Input"

 text .t -wrap word -yscrollcommand {.s set} -state disabled
 scrollbar .s -command {.t yview}
 entry .e -textvariable e

 grid .top -sticky nsew
 grid .bottom -sticky nsew
 grid row . 0 -weight 1
 grid column . 0 -weight 1
 pack .s .t -in .top -side right -fill both
 pack configure .t -expand 1
 pack .e -in .bottom -fill both -expand 1

 focus .e
 update
 bind .e <Return> {sendmessage $e;set e {};break}

 # Connect the GUI to the network
 proc sendmessage {msg} {
     global sock
     if {[string trim $msg] eq ""} { return }
     puts $sock $msg
 }
 proc printmessage {msg} {
     .t conf -state normal
     .t insert end $msg\n
     .t conf -state disabled
     .t see end
 }
 proc receivemessage {sock} {
     if {[gets $sock line] < 0} {
         if {[eof $sock]} {
             close $sock
             printmessage "CONNECTION CLOSED"
         }
     } else {
         printmessage $line
     }
 }

 # Initialise the network
 set sock [socket $host $port]
 fconfigure $sock -encoding iso8859-1 -blocking 0 -buffering line
 fileevent $sock readable [list receivemessage $sock]
