# Словари
dict set colours  colour1 red
puts $colours
dict set colours  colour2 green
puts $colours

set colours [dict create colour1 "black" colour2 "white"]
puts $colours
# Размер словоря
puts "-------------"
set colours [dict create colour1 "black" colour2 "white"]
puts [dict size $colours]
# Итерации словоря
puts "-------------"
set colours [dict create colour1 "black" colour2 "white"]
foreach item [dict keys $colours] {
   set value [dict get $colours $item]
   puts $value
}
# Все ключи словоря
puts "-------------"
set colours [dict create colour1 "black" colour2 "white"]
set keys [dict keys $colours]
puts $keys
# Все значения словоря
puts "-------------"
set colours [dict create colour1 "black" colour2 "white"]
set values [dict values $colours]
puts $values
