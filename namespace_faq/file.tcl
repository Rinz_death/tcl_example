# Работа с файлами
set fp [open "input.txt" w+]
  puts $fp "test"
close $fp
# чтение из файла
puts "------------"

set fp [open "input.txt" r]
  set file_data [read $fp]
  puts $file_data
close $fp

# Чтение несколько строк

puts "-----------------"

set fp [open "input.txt" w+]
  puts $fp "test\ntest"
close $fp

set fp [open "input.txt" r]
  while { [gets $fp data] >= 0 } {
     puts $data
  }
close $fp
