# proc temp {args} {
#   set res 0
#   foreach new_ar $args {
#     set res [expr $res + $new_ar]
#   }
#   puts res
# }
#
# set a { 1 1 23 45 6 78 8 234 2 }
# temp $a

# Ренаме

# rename set var
# rename proc Class
# Class Test {args} {
#   puts $args
# }
# var a {1 2 3 4 5 6}
#
# Test $a
# МЕТАПРОГРАММИМРОВАНИЕ
# proc Add {} {
#   puts "Fack"
# }
# set string "Hello, World"
#
# set cmd [list puts stdout $string]
# puts stdout {Hello, World}
# unset string
# set cmd [proc Add puts stdout $string]
# eval $cmd
proc Socket.Client {host port timeout} {
  global connected
  after $timeout {set connected timeout}
  set sock [socket -async $host $port]
  fileevent $sock w {set connected ok}
  vwait connected
  fileevent $sock w {}
  if {$connected == "timeout"} {
      return -code error timeout
  } else {
      puts $sock
      return $sock
  }
}
Socket.Client "www.tcl.tk" 80 1000
