# процедуры
proc helloWorld {} {
   puts "Hello, World!"
}
helloWorld
# Процедура сложения
puts "--------"
proc add {a b} {
   return [expr $a+$b]
}
puts [add 10 30]
# Нахождение средного значения
puts "--------"
proc avg {numbers} {
   set sum 0
   foreach number $numbers {
      set sum  [expr $sum + $number]
   }
   set average [expr $sum/[llength $numbers]]
   return $average
}
puts [avg {70 80 50 60}]
puts [avg {70 80 50 }]
# Процедуры с условиями
puts "--------"
proc add {a {b 100} } {
   return [expr $a+$b]
}
puts [add 10 30]
puts [add 10]
# рекурсивная процедура
puts "--------"
proc factorial {number} {
   if {$number <= 1} {
      return 1
   }
   return [expr $number * [factorial [expr $number - 1]]]

}
puts [factorial 3]
puts [factorial 5]
#
puts "--------"
